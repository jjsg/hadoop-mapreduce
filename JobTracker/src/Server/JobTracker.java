package Server;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import ServerRMIInterface.IJobTracker;
import ServerRMIInterface.INameNode;



public class JobTracker implements IJobTracker
{
	private static String nameNodeIP="";
	private static Registry namenodeRegistry;
	private static String name = "jobTracker_nameNode";
   // private static String jobTrackerIp="";
    public static INameNode inamenodeRMI ;
    
    
    
	public static void main(String[] args) {
		try
		{
			
			if(args.length!=2)
			{
				System.err.println("Usage: java -Djava.security.policy=server.policy JobTracker <RMI Port No> <Namenode ip>");
				System.exit(1);
			}
			
			nameNodeIP=args[1];
			
			
			if (System.getSecurityManager() == null)
			{
		           System.setSecurityManager(new SecurityManager());
		    }
			
			System.out.println("Security Manager Set");
			
			System.out.println("Looking For jobTracker_nameNode Registry");
			namenodeRegistry = LocateRegistry.getRegistry(nameNodeIP);
			inamenodeRMI = (INameNode) namenodeRegistry.lookup(name);
			
			System.out.println("jobTracker_nameNode Lookup Done");
			
			
			
				IJobTracker nn=new JobTracker();
				IJobTracker stub=(IJobTracker) UnicastRemoteObject.exportObject(nn, 0);
				
					try
					{
						LocateRegistry.createRegistry(Integer.parseInt(args[0]));
					}
					catch(Exception e)
					{
						e.printStackTrace();
						System.out.println("Cant create registry");
						System.exit(1);
					}
				
				Registry reg=LocateRegistry.getRegistry();
				
				String name="jobClient_jobTracker";
				reg.rebind(name, stub);
				System.out.println("jobClient_jobTracker Bound");
				
				String name2="taskTracker_jobTracker";
				reg.rebind(name2, stub);
				System.out.println("taskTracker_jobTracker Bound");
				
				
			/*
				String name2="DataNode_NameNode";
				reg.rebind(name2, stub);
				System.out.println("DataNode_NameNode Bound");*/
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public byte[] jobSubmit(byte[] JobSubmitRequest) {
		String mapName;
		String reducerName;
		String inputFile;
		String outputFile;
		int noReducers;
		MapReduce.JobSubmitResponse.Builder jSubResp=null;
		try {
			
			MapReduce.JobSubmitRequest jSubReq=MapReduce.JobSubmitRequest.parseFrom(JobSubmitRequest);
			mapName=jSubReq.getMapName();
			reducerName=jSubReq.getReducerName();
			inputFile=jSubReq.getInputFile();
			outputFile=jSubReq.getOutputFile();
			noReducers=jSubReq.getNumReduceTasks();
			
			jSubResp=JobTrackerImplementations.jobSubmit(mapName,reducerName,inputFile,outputFile,noReducers);
		}
		catch (Exception e)
		{
			if(jSubResp==null)
			{
				jSubResp=MapReduce.JobSubmitResponse.newBuilder();
			}
			jSubResp.setStatus(1);
			e.printStackTrace();
		}

		return jSubResp.build().toByteArray();
	}

	@Override
	public byte[] getJobStatus(byte[] JobStatusRequest) {
		
		int jobId;
		MapReduce.JobStatusResponse.Builder jStatResp=null;
		try
		{
			MapReduce.JobStatusRequest jStatReq=MapReduce.JobStatusRequest.parseFrom(JobStatusRequest);
			jobId=jStatReq.getJobId();
			
			jStatResp=JobTrackerImplementations.getJobStatus(jobId);
		}
		catch(Exception e)
		{
			if(jStatResp==null)
			{
				jStatResp=MapReduce.JobStatusResponse.newBuilder();
			}
			jStatResp.setStatus(1);
			e.printStackTrace();
			
		}
		return jStatResp.build().toByteArray();
	}

	@Override
	public byte[] heartBeat(byte[] HeartBeatRequest) {
		
		int taskTrackerId;
		int freeMapSlots;
		int freeReducerSlots;
		List<MapReduce.MapTaskStatus> mapStatus;
		List<MapReduce.ReduceTaskStatus> reducerStatus;
		
		MapReduce.HeartBeatResponse.Builder hbResp=null;
		try
		{
			MapReduce.HeartBeatRequest hbReq=MapReduce.HeartBeatRequest.parseFrom(HeartBeatRequest);
			taskTrackerId=hbReq.getTaskTrackerId();
			freeMapSlots=hbReq.getNumMapSlotsFree();
			freeReducerSlots=hbReq.getNumReduceSlotsFree();
			mapStatus=hbReq.getMapStatusList();
			reducerStatus=hbReq.getReduceStatusList();
			
			hbResp=JobTrackerImplementations.heartBeat(taskTrackerId,freeMapSlots,freeReducerSlots,mapStatus,reducerStatus);
		}
		catch(Exception e)
		{
			if(hbResp==null)
			{
				hbResp=MapReduce.HeartBeatResponse.newBuilder();
			}
			hbResp.setStatus(1);
			e.printStackTrace();
			
		}
		
		return hbResp.build().toByteArray();
	}

}
