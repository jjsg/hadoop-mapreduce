package Server;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import Server.MapReduce.HeartBeatResponse;
import Server.MapReduce.JobSubmitResponse.Builder;
import Server.MapReduce.MapTaskStatus;
import Server.MapReduce.ReduceTaskStatus;
import ServerRMIInterface.INameNode;

public class JobTrackerImplementations {
	static INameNode inamenodeRMI;
	static Map<Integer,JobDetails> jobsInfo;
	static Queue<MapReduce.MapTaskInfo.Builder> mapperQueue;
	static Queue<MapReduce.ReducerTaskInfo.Builder> reducerQueue;
	static Integer jobId;
	static
	{
		inamenodeRMI=JobTracker.inamenodeRMI;
		jobsInfo=new HashMap<Integer, JobDetails>();
		mapperQueue=new LinkedList<MapReduce.MapTaskInfo.Builder>();
		reducerQueue=new LinkedList<MapReduce.ReducerTaskInfo.Builder>();
		jobId=0;
	}
	
	public static Builder jobSubmit(String mapName, String reducerName,
			String inputFile, String outputFile, int noReducers) throws Exception {
		
		int jobid,taskId=0;
		MapReduce.JobSubmitResponse.Builder jSubResp=MapReduce.JobSubmitResponse.newBuilder();
		HDFS.BlockLocationResponse blresp=read(inputFile);
		if(blresp==null || blresp.getStatus()==1)
		{
			jSubResp.setStatus(1);
			return jSubResp;
		}
		synchronized(jobId)
		{
			jobId++;
			jobid=jobId;
		}
		
		MapReduce.MapTaskInfo.Builder mtaskInfo;
		for(HDFS.BlockLocations bloc:blresp.getBlockLocationsList())
		{
			taskId++;
			mtaskInfo=MapReduce.MapTaskInfo.newBuilder();
			mtaskInfo.setJobId(jobid);
			mtaskInfo.setTaskId(taskId);
			mtaskInfo.setMapName(mapName);
			mtaskInfo.setNumReduceTasks(noReducers);
			System.out.println("Error can come since type casted from HDFS.block to Mapreduce.block");
			byte[] temp=bloc.toByteArray();
			mtaskInfo.addInputBlocks(MapReduce.BlockLocations.parseFrom(temp));
			synchronized (mapperQueue) {
				mapperQueue.add(mtaskInfo);
			}
		}
		
		JobDetails jd=new JobDetails(jobid, mapName, reducerName, inputFile, outputFile, noReducers, taskId);
		jd.setTaskId(taskId);
		synchronized (jobsInfo) {
			jobsInfo.put(jobId, jd);
		}
		jSubResp.setJobId(jobid);
		jSubResp.setStatus(0);
		
		return jSubResp;
	}
	
	private static HDFS.BlockLocationResponse read(String fileName)
	{
		 try 
		 {
			 System.out.println("\nIn Read\n");
			 //open
			 HDFS.OpenFileResponse ofresp=open(fileName,true);
	        
	         if(ofresp.getStatus()==0)
	         {
	        	
	        	 System.out.println("Open Request Successful");
	        	 int handle=ofresp.getHandle();
	        	
	        	 //block location request
	        	 System.out.println("Block Location Request");
		         HDFS.BlockLocationRequest.Builder blreqBuilder= HDFS.BlockLocationRequest.newBuilder();
		         HDFS.BlockLocationResponse blresp=null;
	        	 blreqBuilder.addAllBlockNums(ofresp.getBlockNumsList());
	        	 int blCount=3;
	        	 do
	        	 {
		        	 System.out.println("Block Location Attempt"+blCount);

		        	 byte[] blrespByte=inamenodeRMI.getBlockLocations(blreqBuilder.build().toByteArray());
		        	 blresp=HDFS.BlockLocationResponse.parseFrom(blrespByte);
		        	 
		        	 if(blresp.getStatus()==0)
		        	 {
		        		 System.out.println("Block Location Request Successful");
		        		 break;
		        	 }	  
		        	 blCount--;
		         }
	        	 while(blCount>0);
	        	 if(blCount==0)
	        	 {
	        		 System.out.println("Block Location Error");
	        		 close(handle);
	        		 System.out.println("\nRead Failed\n");
	        		 return null;
	        	 }
	        	 else
	        	 {
		        	 //close
		        	 if(close(handle))
		        	 {
		        		 System.out.println("\nDone Reading Successfully\n");
		        		 
		        	 }
		        	 else
		        	 {
		        		 System.out.println("\nDone Reading But Could Not Close File\n");
			         }
		        	 return blresp;
		        	 
	        	 }
	        	 
	         }
	         else
	         {
	        	 System.out.println("Open File Error");
	        	 return null;
	         }
		 }
		 catch (Exception e)
		 {
			 System.err.println("Exception In Reading");
	         e.printStackTrace();
		 }
		return null;
	}

	private static HDFS.OpenFileResponse open(String fileName,boolean forRead) throws Exception
	{
		 //open
		int openCount=3;
		HDFS.OpenFileRequest.Builder ofreqBuilder= HDFS.OpenFileRequest.newBuilder();
		ofreqBuilder.setFileName(fileName);
	    ofreqBuilder.setForRead(forRead);
	    
		System.out.println("Open Request");
		HDFS.OpenFileResponse ofresp;
		do
		{
				 System.out.println("Open Request Attempt:"+openCount);
				 byte[] ofrespByte=inamenodeRMI.openFile(ofreqBuilder.build().toByteArray());
				 ofresp=HDFS.OpenFileResponse.parseFrom(ofrespByte);
				 if(ofresp.getStatus()==0)    
					 return ofresp;
				 openCount--;   
		}while(openCount!=0);
		
		return ofresp;
	   
	}
	private static boolean close(int handle) throws Exception
	{
		 //close
	   	 System.out.println("Close Request");
		 HDFS.CloseFileRequest.Builder cfreqBuilder=HDFS.CloseFileRequest.newBuilder();
	   	 cfreqBuilder.setHandle(handle);
	   	 
	   	 int closeCount=3;
	   	
			 do
	   	 {
				 System.out.println("Close Request Attempt:"+closeCount);
	   		 byte[] cfrespByte=inamenodeRMI.closeFile(cfreqBuilder.build().toByteArray());
	   		 HDFS.CloseFileResponse cfresp=HDFS.CloseFileResponse.parseFrom(cfrespByte);
	       	 if(cfresp.getStatus()==0)
	       	 {
	       		 System.out.println("Close Request Successful");
	       		 return true;
	       	 }
	       	 closeCount--;
	   	 }while(closeCount>0);
	   	
			 System.out.println("Close Request Error");
	   	 return false;
		
	}

	public static Server.MapReduce.JobStatusResponse.Builder getJobStatus(
			int jobId2) throws Exception
	{
		JobDetails jd;
		synchronized (jobsInfo) {
			jd=jobsInfo.get(jobId2);
		}
		MapReduce.JobStatusResponse.Builder jStatResp=MapReduce.JobStatusResponse.newBuilder();
		jStatResp.setTotalMapTasks(jd.getNoMappers());
		jStatResp.setTotalReduceTasks(jd.getNoReducers());
		jStatResp.setNumMapTasksStarted(jd.getMappersStarted());
		jStatResp.setNumReduceTasksStarted(jd.getReducersStarted());
		if(jd.getReducerCompleted()==jd.getNoReducers())
		{
			jStatResp.setJobDone(true);
			synchronized (jobsInfo) {
				jobsInfo.remove(jobId2);
			}
			
		}
		else 
		{
			jStatResp.setJobDone(false);
		} 
		
		if(System.currentTimeMillis()-jd.getLastUpdationTime()>600000)//10mins
			jStatResp.setStatus(1);
		else
			jStatResp.setStatus(0);
		return jStatResp;
	}

	public static Server.MapReduce.HeartBeatResponse.Builder heartBeat(
			int taskTrackerId, int freeMapSlots, int freeReducerSlots,
			List<MapTaskStatus> mapStatus, List<ReduceTaskStatus> reducerStatus) {
		
			int jobId;
			int taskId;
			boolean completed;
			String output;
			JobDetails jd;
			if(mapStatus!=null)
			{
				for(MapReduce.MapTaskStatus mapStat:mapStatus)
				{
					jobId=mapStat.getJobId();
					synchronized (jobsInfo) {
						jd=jobsInfo.get(jobId);
					}
					taskId=mapStat.getTaskId();
					completed=mapStat.getTaskCompleted();
					if(completed)
					{
						output=mapStat.getMapOutputFile();
						boolean x=(jd.getAssignedTTId()).remove(new Integer(taskTrackerId));
						if(x)
						{
							jd.setMapCompleted(jd.getMapCompleted()+1);
							jd.getMapOutputFiles().add(output);
							if(jd.getMapCompleted()==jd.getNoMappers())
							{
								MapReduce.ReducerTaskInfo.Builder reducerInfo;
								for(int i=1;i<=jd.getNoReducers();i++)
								{
									jd.setTaskId(jd.getTaskId()+1);
									reducerInfo=MapReduce.ReducerTaskInfo.newBuilder();
									reducerInfo.setJobId(jobId);
									reducerInfo.setReducerNo(i);
									reducerInfo.setTaskId(jd.getTaskId());
									reducerInfo.setReducerName(jd.getReducerName());
									reducerInfo.addAllMapOutputFiles(jd.getMapOutputFiles());
									reducerInfo.setOutputFile(jd.getOutputFile());
									synchronized (reducerQueue) {
										reducerQueue.add(reducerInfo);
									}
								}
							}
						}
						else
						{
							System.out.println("Lovlean Saying Ouchhhhhhhhhh Ahhhhhhhhhhhh");
						}
					}
					jd.setLastUpdationTime(System.currentTimeMillis());
				}
			}
			
			if(reducerStatus!=null)
			{
				for(MapReduce.ReduceTaskStatus reducerStat:reducerStatus)
				{
					jobId=reducerStat.getJobId();
					synchronized (jobsInfo) {
						jd=jobsInfo.get(jobId);
					}
					taskId=reducerStat.getTaskId();
					completed=reducerStat.getTaskCompleted();
					if(completed)
					{
						boolean x=(jd.getAssignedTTId()).remove(new Integer(taskTrackerId));
						if(x)
						{
							jd.setReducerCompleted(jd.getReducerCompleted()+1);
						}
						else
						{
							System.out.println("Lovlean Saying Ouchhhhhhhhhh Ahhhhhhhhhhhh");
						}
					}
					jd.setLastUpdationTime(System.currentTimeMillis());
				}
			}
			
			MapReduce.HeartBeatResponse.Builder hbResp=MapReduce.HeartBeatResponse.newBuilder();
			MapReduce.MapTaskInfo.Builder mapInfo=null;
			if(freeMapSlots>0)
			{
				synchronized (mapperQueue) {
					mapInfo=mapperQueue.poll(); //If Queue is Empty returns NULL TODO
				}
				if(mapInfo!=null)
				{
					jobId=mapInfo.getJobId();
					synchronized (jobsInfo) {
						jd=jobsInfo.get(jobId);
					}
					jd.setMappersStarted(jd.getMappersStarted()+1);
					jd.getAssignedTTId().add(taskTrackerId);
					jd.setLastUpdationTime(System.currentTimeMillis());
					hbResp.addMapTasks(mapInfo);
				}
			}
			
			
			MapReduce.ReducerTaskInfo.Builder reducerInfo=null;
			if(freeReducerSlots>0)
			{
				synchronized (reducerQueue) {
					reducerInfo=reducerQueue.poll();
				}
				if(reducerInfo!=null)
				{
						jobId=reducerInfo.getJobId();
						synchronized (jobsInfo) {
							jd=jobsInfo.get(jobId);
						}
						jd.setReducersStarted(jd.getReducersStarted()+1);
						jd.getAssignedTTId().add(taskTrackerId);
						jd.setLastUpdationTime(System.currentTimeMillis());
						hbResp.addReduceTasks(reducerInfo);
				}
			}
			hbResp.setStatus(0);
			
		
		
			return hbResp;
	}

}
