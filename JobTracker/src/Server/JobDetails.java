package Server;

import java.util.ArrayList;

public class JobDetails {

	private int jobId;
	private String mapName;
	private String reducerName;
	private String inputFile;
	private String outputFile;
	private int noReducers;
	private int noMappers;
	private int mappersStarted;
	private int reducersStarted;
	private ArrayList<String> mapOutputFiles;
	private ArrayList<Integer> assignedTTId;
	private long lastUpdationTime;
	private int taskId;
	private int mapCompleted;
	private int reducerCompleted;
	
	public JobDetails(int jobid,String mapname, String reducername,
			String inputfile, String outputfile, int noreducers,int nomappers)
	{
		jobId=jobid;
		mapName=mapname;
		reducerName=reducername;
		inputFile=inputfile;
		outputFile=outputfile;
		noReducers=noreducers;
		noMappers=nomappers;
		mappersStarted=0;
		reducersStarted=0;
		mapOutputFiles=new ArrayList<String>();
		assignedTTId=new ArrayList<Integer>();
		taskId=0;
		mapCompleted=0;
		reducerCompleted=0;
		lastUpdationTime=System.currentTimeMillis();
	}

	public int getMapCompleted() {
		return mapCompleted;
	}

	public void setMapCompleted(int mapCompleted) {
		this.mapCompleted = mapCompleted;
	}

	public int getReducerCompleted() {
		return reducerCompleted;
	}

	public void setReducerCompleted(int reducerCompleted) {
		this.reducerCompleted = reducerCompleted;
	}

	public ArrayList<Integer> getAssignedTTId() {
		return assignedTTId;
	}

	public void setAssignedTTId(ArrayList<Integer> assignedTTId) {
		this.assignedTTId = assignedTTId;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public String getReducerName() {
		return reducerName;
	}

	public void setReducerName(String reducerName) {
		this.reducerName = reducerName;
	}

	public String getInputFile() {
		return inputFile;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	public int getNoReducers() {
		return noReducers;
	}

	public void setNoReducers(int noReducers) {
		this.noReducers = noReducers;
	}

	public int getNoMappers() {
		return noMappers;
	}

	public void setNoMappers(int noMappers) {
		this.noMappers = noMappers;
	}

	public int getMappersStarted() {
		return mappersStarted;
	}

	public void setMappersStarted(int mappersStarted) {
		this.mappersStarted = mappersStarted;
	}

	public int getReducersStarted() {
		return reducersStarted;
	}

	public void setReducersStarted(int reducersStarted) {
		this.reducersStarted = reducersStarted;
	}

	public ArrayList<String> getMapOutputFiles() {
		return mapOutputFiles;
	}

	public void setMapOutputFiles(ArrayList<String> mapOutputFiles) {
		this.mapOutputFiles = mapOutputFiles;
	}

	

	public long getLastUpdationTime() {
		return lastUpdationTime;
	}

	public void setLastUpdationTime(long lastUpdationTime) {
		this.lastUpdationTime = lastUpdationTime;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
}
