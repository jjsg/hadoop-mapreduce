#!/bin/bash

if [ $# -eq 1 ] && [ $1 == "-c" ]
then
	mkdir bin >/dev/null 2>/dev/null
	javac -cp jars/protobuf-java-2.6.0.jar -d bin/ src/*/*.java
elif [ $# -eq 3 ] && [ $1 == "-r" ]
then
	java -cp bin/:jars/protobuf-java-2.6.0.jar -Djava.security.policy=server.policy Server.JobTracker $2 $3		
else
 	echo "Usage: bash JobTracker.sh -c //to compile"
	echo "       bash JobTracker.sh -r <RMI port> <NameNode IP>" 			
fi

