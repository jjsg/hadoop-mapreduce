
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Server.MapReduce.HeartBeatResponse;
import Server.*;


public class TaskTrackerImplementations {

	public static Map<String,MapReduce.MapTaskStatus.Builder> mapDetails;
	public static Map<String,MapReduce.ReduceTaskStatus.Builder> reducerDetails;
	public static ArrayList<String> sentMapCompleted;
	public static ArrayList<String> sentReducerCompleted;
	public static ExecutorService mapExecService;
	public static ExecutorService reducerExecService;
	public static int allowedThreads;
	
	static
	{
		allowedThreads=5;
		mapDetails=new HashMap<String, MapReduce.MapTaskStatus.Builder>();
		reducerDetails=new HashMap<String, MapReduce.ReduceTaskStatus.Builder>();
		mapExecService=Executors.newFixedThreadPool(allowedThreads);
		reducerExecService=Executors.newFixedThreadPool(allowedThreads);//No of thread that will be running Reducer
		sentMapCompleted=new ArrayList<String>();
		sentReducerCompleted=new ArrayList<String>();
	}
	
	public static MapReduce.HeartBeatRequest.Builder heartbeatRequestGenerator() {
		
		MapReduce.HeartBeatRequest.Builder hbReq=MapReduce.HeartBeatRequest.newBuilder();
		int usedcount=0;
		try
		{
			hbReq.setTaskTrackerId(TaskTracker.uniqueId);
			sentMapCompleted.clear();
			synchronized (mapDetails) {
			
				MapReduce.MapTaskStatus.Builder mapStat;
				for(String key:mapDetails.keySet())
				{
						mapStat=mapDetails.get(key);
						if(mapStat.getTaskCompleted())
							sentMapCompleted.add(key);
						else
							usedcount++;
						hbReq.addMapStatus(mapStat);
				}
			}
			hbReq.setNumMapSlotsFree(allowedThreads-usedcount);
			
			usedcount=0;
			sentReducerCompleted.clear();
			synchronized (reducerDetails) {
			
				MapReduce.ReduceTaskStatus.Builder reducerStat;
				for(String key:reducerDetails.keySet())
				{
					reducerStat=reducerDetails.get(key);
						if(reducerStat.getTaskCompleted())
							sentReducerCompleted.add(key);
						else
							usedcount++;
						hbReq.addReduceStatus(reducerStat);
				}
			}
			hbReq.setNumReduceSlotsFree(allowedThreads-usedcount);
			return hbReq;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}

	public static void heartbeatResponseProcessor(HeartBeatResponse hbResp) {
		
		//Remove completed and sent tasks
		for(String s:sentMapCompleted)
		{
			synchronized (mapDetails) {
				mapDetails.remove(s);	
			}
		}
		
		for(String s:sentReducerCompleted)
		{
			synchronized (reducerDetails) {
				reducerDetails.remove(s);	
			}
		}
		
		int jobid,taskid;
		MapReduce.MapTaskStatus.Builder mapStat;
		for(MapReduce.MapTaskInfo mapInfo:hbResp.getMapTasksList())
		{
			jobid=mapInfo.getJobId();
			taskid=mapInfo.getTaskId();
			mapStat=MapReduce.MapTaskStatus.newBuilder();
			mapStat.setJobId(jobid);
			mapStat.setTaskId(taskid);
			mapStat.setTaskCompleted(false);
			synchronized (mapDetails) {
				mapDetails.put(jobid+"_"+taskid,mapStat);	
			}
			MapWorker mw=new MapWorker(jobid, taskid, mapInfo.getMapName(),mapInfo.getNumReduceTasks(),mapInfo.getInputBlocksList());
			mapExecService.execute(mw);
			
		}
		
		MapReduce.ReduceTaskStatus.Builder reducerStat;
		for(MapReduce.ReducerTaskInfo reduceInfo:hbResp.getReduceTasksList())
		{
			jobid=reduceInfo.getJobId();
			taskid=reduceInfo.getTaskId();
			reducerStat=MapReduce.ReduceTaskStatus.newBuilder();
			reducerStat.setJobId(jobid);
			reducerStat.setTaskId(taskid);
			reducerStat.setTaskCompleted(false);
			synchronized (reducerDetails) {
				reducerDetails.put(jobid+"_"+taskid,reducerStat);	
			}
			
			ArrayList<String> a=new ArrayList<String>();
			a.addAll(reduceInfo.getMapOutputFilesList());
			ReduceWorker rw=new ReduceWorker(jobid, taskid, reduceInfo.getReducerNo(), reduceInfo.getReducerName(), reduceInfo.getOutputFile(), a);
			reducerExecService.execute(rw);
			
		}
		
	}
	

}
