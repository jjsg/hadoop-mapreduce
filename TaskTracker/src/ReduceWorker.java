
import java.io.File;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLClassLoader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Server.HDFS;
import Server.MapReduce;
import ServerRMIInterface.IDataNode;

import com.google.protobuf.ByteString;

public class ReduceWorker implements Runnable{

	private int jobId,taskId,reducerNo;
	private String reducerName,outputFile;
	private ArrayList<String> mapOutputFiles;
	private static int BlockSize=16*1024*1024;
	
	
	public ReduceWorker(int jobId, int taskId, int reducerNo,
			String reducerName, String outputFile,
			ArrayList<String> mapOutputFiles) {
		super();
		this.jobId = jobId;
		this.taskId = taskId;
		this.reducerNo = reducerNo;
		this.reducerName = reducerName;
		this.outputFile = outputFile;
		this.mapOutputFiles = mapOutputFiles;
	}

	@Override
	public void run() {
		
		try
		{
			PrintWriter pw= new PrintWriter(outputFile+"_"+jobId+"_"+taskId);
			
			File gameJar = new File("jars/MapperReducer.jar");
	        URLClassLoader cl = new URLClassLoader(new URL[]
		                {
		                    gameJar.toURI().toURL(),
		                });
	
		    Class clazz = Class.forName(reducerName, true, cl);
	        IReducer myClass = (IReducer)clazz.newInstance();
	        	
		StringBuilder temp2=new StringBuilder();
	        for(String s:mapOutputFiles)
			{
				String temp=read(s+"_"+reducerNo);
				String lines[]=temp.split("\n");
				temp2.setLength(0);
			for(String line:lines)
	        	{
	        		String ref=myClass.reduce(line);
	        		if(ref!=null)
	        			temp2.append(ref+"\n");
	        	}
				pw.write(temp2.toString());
			}
	        pw.close();
	        write(outputFile+"_"+jobId+"_"+taskId);
		
	        synchronized (TaskTrackerImplementations.reducerDetails) {
				MapReduce.ReduceTaskStatus.Builder reducerStat=TaskTrackerImplementations.reducerDetails.get(jobId+"_"+taskId);
				reducerStat.setTaskCompleted(true);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
				
	}

	
	private static String read(String fileName)
	{
		 String result="";
		 
		 try 
		 {
			 System.out.println("\nIn Read\n");
			 //open
			 HDFS.OpenFileResponse ofresp=open(fileName,true);
	        
	         if(ofresp.getStatus()==0)
	         {
	        	
	        	 System.out.println("Open Request Successful");
	        	 int handle=ofresp.getHandle();
	        	
	        	 //block location request
	        	 System.out.println("Block Location Request");
		         HDFS.BlockLocationRequest.Builder blreqBuilder= HDFS.BlockLocationRequest.newBuilder();
	        	 blreqBuilder.addAllBlockNums(ofresp.getBlockNumsList());
	        	 int blCount=3;
	        	 do
	        	 {
		        	 System.out.println("Block Location Attempt"+blCount);

		        	 byte[] blrespByte=TaskTracker.nameNodeRMI.getBlockLocations(blreqBuilder.build().toByteArray());
		        	 HDFS.BlockLocationResponse blresp=HDFS.BlockLocationResponse.parseFrom(blrespByte);
		        	 
		        	 if(blresp.getStatus()==0)
		        	 {
		        		 System.out.println("Block Location Request Successful");
		        		 List<HDFS.DataNodeLocation> availableDatanodeIP;
		        		 HDFS.DataNodeLocation dnl;
		        		 HDFS.ReadBlockRequest.Builder rbreqBuilder;
		        		 HDFS.ReadBlockResponse rbres;
		        		 byte[] rbresByte;
		        		 String datanodeIP;
		        		 int blockNumber;
		        		 int rand;
		        		 Random randomGenerator = new Random();
		        		 for(HDFS.BlockLocations bl:blresp.getBlockLocationsList())
		        		 {
		        			 blockNumber=bl.getBlockNumber();
		        			 availableDatanodeIP=bl.getLocationsList();
		        			 if(availableDatanodeIP.size()==0)
		        			 {
		        				 System.out.println("No Available Node to read, Block Read Error "+blockNumber);
		        				 return result;
		        			 }
		        			 
		        			 int readCount=3;
		        			 do
		        			 {
			        			 rand=randomGenerator.nextInt(availableDatanodeIP.size());
			        			 dnl=bl.getLocations(rand);
			        			 datanodeIP=convertIntIp2String(dnl.getIp());
			        			 
			        			 //datanode registry
			        			 System.out.println("Looking For Client_DataNode Registry");
			        			 Registry datanodeRegistry = LocateRegistry.getRegistry(datanodeIP);
			        			 String name1 = "Client_DataNode";
			        		     IDataNode idatanodeRMI = (IDataNode) datanodeRegistry.lookup(name1);
			        		     
			        		     System.out.println("Client_DataNode Lookup Successful");
			        		     
			        		     //read
			        		     System.out.println("Read Request at DataNode"+datanodeIP+" attempt "+readCount);
			        		     rbreqBuilder=HDFS.ReadBlockRequest.newBuilder();
			        		     rbreqBuilder.setBlockNumber(blockNumber);
			        		     rbresByte=idatanodeRMI.readBlock(rbreqBuilder.build().toByteArray());
			        		     rbres=HDFS.ReadBlockResponse.parseFrom(rbresByte);
			        		     
			        		     if(rbres.getStatus()==0)
			        		     {
			        		    	System.out.println("Block Read Request Successful "+blockNumber);
			        		    	result=result+new String(rbres.getData().toByteArray());
			        		    	break;		        		    	
			        		     }
			        		     readCount--;
			        		    
			        		}while(readCount>0);
		        			 
		        			 if(readCount==0)
		        			 {
		        				 System.out.println("Block Read Error "+blockNumber);
		        				 break;
		        			 }
			        		  
		        		 }
		        		 break;
		        	 }
		        	 blCount--;
		         }
	        	 while(blCount>0);
	        	 if(blCount==0)
	        	 {
	        		 System.out.println("Block Location Error");
	        		 close(handle);
	        		 System.out.println("\nRead Failed\n");
	        	 }
	        	 else
	        	 {
		        	 //close
		        	 if(close(handle))
		        	 {
		        		 System.out.println("\nDone Reading Successfully\n");
		        	 }
		        	 else
		        	 {
		        		 System.out.println("\nDone Reading But Could Not Close File\n");
			         }
	        	 }
	         }
	         else
	         {
	        	 System.out.println("Open File Error");
	         }
		 }
		 catch (Exception e)
		 {
			 System.err.println("Exception In Reading");
	         e.printStackTrace();
		 }
		 return result;
	}
	
	private static HDFS.OpenFileResponse open(String fileName,boolean forRead) throws Exception
	{
		 //open
		int openCount=3;
		HDFS.OpenFileRequest.Builder ofreqBuilder= HDFS.OpenFileRequest.newBuilder();
		ofreqBuilder.setFileName(fileName);
	    ofreqBuilder.setForRead(forRead);
	    
		System.out.println("Open Request");
		HDFS.OpenFileResponse ofresp;
		do
		{
  			 System.out.println("Open Request Attempt:"+openCount);
  			 byte[] ofrespByte=TaskTracker.nameNodeRMI.openFile(ofreqBuilder.build().toByteArray());
  			 ofresp=HDFS.OpenFileResponse.parseFrom(ofrespByte);
  			 if(ofresp.getStatus()==0)    
		    	return ofresp;
  			 openCount--;   
		}while(openCount!=0);
		
		return ofresp;
       
	}
	private static boolean close(int handle) throws Exception
	{
		 //close
	   	 System.out.println("Close Request");
		 HDFS.CloseFileRequest.Builder cfreqBuilder=HDFS.CloseFileRequest.newBuilder();
	   	 cfreqBuilder.setHandle(handle);
	   	 
	   	 int closeCount=3;
	   	
   		 do
	   	 {
   			 System.out.println("Close Request Attempt:"+closeCount);
	   		 byte[] cfrespByte=TaskTracker.nameNodeRMI.closeFile(cfreqBuilder.build().toByteArray());
	   		 HDFS.CloseFileResponse cfresp=HDFS.CloseFileResponse.parseFrom(cfrespByte);
	       	 if(cfresp.getStatus()==0)
	       	 {
	       		 System.out.println("Close Request Successful");
	       		 return true;
	       	 }
	       	 closeCount--;
	   	 }while(closeCount>0);
	   	
   		 System.out.println("Close Request Error");
	   	 return false;
   	
	}
	
	public static String convertIntIp2String(int ip)
	{
		StringBuilder sb=new StringBuilder();
		int temp,maxValue=256*256*256;
		
		for(int i=0;i<=3;i++)
		{
			temp=ip/maxValue;
			sb.append(temp+".");
			ip=ip%maxValue;
			maxValue/=256;
		}
		sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	
	private static void write(String fileName) 
	{
		System.out.println("\nIn Write\n");
		RandomAccessFile raf=null;
		try
		{
			raf=new RandomAccessFile(fileName, "r");
			try
			{
				//open
				File f=new File(fileName);
				HDFS.OpenFileResponse ofresp=open(f.getName(),false);
				if(ofresp.getStatus()==0)
		        {
					System.out.println("Open Request Successful");
			    	int handle=ofresp.getHandle();
			    	int n=-1;
			    	byte[] data;
			    	
			    	data=new byte[BlockSize];
		    		n=raf.read(data);
		    		System.out.println("Block Read Locally");
			    	boolean writeSuccessful=true;
		    		while(n!=-1)
			    	{
			    		//assign block request
			    		System.out.println("Assign Block Request");
			    		HDFS.AssignBlockRequest.Builder abreqBuilder=HDFS.AssignBlockRequest.newBuilder();
			    		abreqBuilder.setHandle(handle);
			    		
			    		int abCount=3;
			    		do
			    		{
			    			System.out.println("Assign Block Request Attempt "+abCount);
			    		
				    		byte[] abrespByte=TaskTracker.nameNodeRMI.assignBlock(abreqBuilder.build().toByteArray());
				    		HDFS.AssignBlockResponse abresp=HDFS.AssignBlockResponse.parseFrom(abrespByte);
				    		
				    		if(abresp.getStatus()==0)
					        {
				    			System.out.println("Assign Block Request Successful");
				    			String firstDNIP=convertIntIp2String(abresp.getNewBlock().getLocations(0).getIp());
				    			
				    			//datanode registry
				    			System.out.println("Looking For Client_DataNode Registry");
			        			Registry datanodeRegistry = LocateRegistry.getRegistry(firstDNIP);
			        			String name1 = "Client_DataNode";
			        		    IDataNode idatanodeRMI = (IDataNode) datanodeRegistry.lookup(name1);
			        		    
			        		    System.out.println("Client_DataNode Lookup Successful");
			        		    
			        		    //write
			        		    System.out.println("Write Request At: "+firstDNIP);
			        		    HDFS.WriteBlockRequest.Builder wbreqBuilder=HDFS.WriteBlockRequest.newBuilder();
			        		    wbreqBuilder.setBlockInfo(abresp.getNewBlock());
			        		    wbreqBuilder.setData(ByteString.copyFrom(data,0,n));
			        		    
			        		    int writeCount=3;
			        		   
			        		   	 do
			        		   	 {
			        		   		 System.out.println("Write Request Attempt"+writeCount);
			        		   		 byte[] wbrespByte=idatanodeRMI.writeBlock(wbreqBuilder.build().toByteArray());
					        		 HDFS.WriteBlockResponse wbresp=HDFS.WriteBlockResponse.parseFrom(wbrespByte);
					        		 if(wbresp.getStatus()==0)
			        		       	 {
					        			 System.out.println("Write Request Successful");
					        			 break;
			        		       	 }
					        		 writeCount--;
			        		   	 }while(writeCount>0);
			        		   	 
			        		   	 if(writeCount==0)
			        		   	 {
			        		   		 System.out.println("Write Block Error");
			        		   		 writeSuccessful=false;
			        		   	 }
			        		   	 break; 
					        }
				    		abCount--;
			        	}
			    		while(abCount!=0);
			    		if(abCount==0)	
			    		{
			    			System.out.println("Assign Block Error");
			    			writeSuccessful=false;
			    			break;
			    		}
			    		
			    		data=new byte[BlockSize];
			    		n=raf.read(data);
			    		System.out.println("Block Read Locally");
			    	}
			    	
		    		//close
		        	 if(writeSuccessful)
		        	 {
		        		 if(close(handle))
		        		 {
		        			 System.out.println("\nDone Writing Successfully\n");
		        		 }
		        		 else
		        		 {
		        			 System.out.println("\nDone Writing But Could Not Close File\n");
		        		 }
		        	}
		        	 else
		        	 {
	        			 System.out.println("\nWriting Failed\n");
		        	 }
		        	
			    	
		        }
			    else
		        {
					System.out.println("Open File Error");
		        }
			}
			catch (Exception e)
			{
				try{
					if(raf!=null)
					{
						raf.close();
					}
				}catch(Exception e1)
				{
					
				}
				System.err.println("Exception In Writing");
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			try{
				if(raf!=null)
				{
					raf.close();
				}
			}catch(Exception e1)
			{
				
			}
			System.err.println("Exception In Opening File Locally");
			e.printStackTrace();
		}
		
	}

	
}
