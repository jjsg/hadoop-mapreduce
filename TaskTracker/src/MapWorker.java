
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ServerRMIInterface.IDataNode;
import Server.*;
import com.google.protobuf.ByteString;

public class MapWorker implements Runnable{

	private int jobId,taskId,noReducers;
	private String mapName,outputFile;
	private List<MapReduce.BlockLocations> blockLoc;
	private String[] files ;
	private static int BlockSize=16*1024*1024;
	
	public MapWorker(int jobId,int taskId,String mapName,int noReducers,List<MapReduce.BlockLocations> blockLoc) {
	
		this.jobId=jobId;
		this.taskId=taskId;
		this.mapName=mapName;
		this.noReducers=noReducers;
		this.blockLoc=blockLoc;
		files=new String[noReducers];
		
		for(int i=0;i<noReducers;i++)
			files[i]="";
	}
	
	@Override
	public void run() {
		
		ArrayList<String> readString;
		try
		{
			readString=read();
			File gameJar = new File("jars/MapperReducer.jar");
	        URLClassLoader cl = new URLClassLoader(new URL[]
		                {
		                    gameJar.toURI().toURL(),
		                });

		    Class clazz = Class.forName(mapName, true, cl);
	        IMapper myClass = (IMapper)clazz.newInstance();
	        
	        String fileName=jobId+"_"+taskId;
		StringBuilder temp=new StringBuilder();
	        for(String s : readString)
	        {
			
	        	String lines[]=s.split("\n");
			int len=lines.length;
			System.out.println("mapping"+lines.length);	        	
			for(int i=0;i<noReducers;i++)
			{
				temp.setLength(0);
				for(int j=i;j<len;j+=noReducers)
				{
		        		String temp2=myClass.map(lines[j]);
		        		if(temp2!=null)
	        				temp.append(temp2+"\n");
				}
				write(fileName+"_"+(i+1), temp.toString());
			}
			System.out.println("mapping_done");
	        }
	        outputFile=fileName;
	        synchronized (TaskTrackerImplementations.mapDetails) {
				MapReduce.MapTaskStatus.Builder mapStat=TaskTrackerImplementations.mapDetails.get(fileName);
				mapStat.setTaskCompleted(true);
				mapStat.setMapOutputFile(outputFile);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public ArrayList<String> read() throws Exception
	{
		 List<MapReduce.DataNodeLocation> availableDatanodeIP;
		 MapReduce.DataNodeLocation dnl;
		 HDFS.ReadBlockRequest.Builder rbreqBuilder;
		 HDFS.ReadBlockResponse rbres;
		 ArrayList<String> readStrings;
		 byte[] rbresByte;
		 String datanodeIP;
		 int blockNumber;
		 int rand;
		 Random randomGenerator = new Random();
		 readStrings=new ArrayList<String>();
		 for(MapReduce.BlockLocations bl:blockLoc)
		 {
			 blockNumber=bl.getBlockNumber();
			 availableDatanodeIP=bl.getLocationsList();
			 if(availableDatanodeIP.size()==0)
			 {
				 System.out.println("No Available Node to read, Block Read Error "+blockNumber);
				 return readStrings;
			 }
			 
			 rand=randomGenerator.nextInt(availableDatanodeIP.size());
   			 dnl=bl.getLocations(rand);
   			 datanodeIP=convertIntIp2String(dnl.getIp());
   			 
   			 //datanode registry
   			 System.out.println("Looking For taskTracker_dataNode Registry");
   			 Registry datanodeRegistry = LocateRegistry.getRegistry(datanodeIP);
   			 String name1 = "taskTracker_dataNode";
   		     IDataNode idatanodeRMI = (IDataNode) datanodeRegistry.lookup(name1);
   		     
   		     System.out.println("taskTracker_dataNode Lookup Successful");
   		     
   		     //read
   		     rbreqBuilder=HDFS.ReadBlockRequest.newBuilder();
   		     rbreqBuilder.setBlockNumber(blockNumber);
   		     rbresByte=idatanodeRMI.readBlock(rbreqBuilder.build().toByteArray());
   		     rbres=HDFS.ReadBlockResponse.parseFrom(rbresByte);
   		     
   		     if(rbres.getStatus()==0)
   		     {
   		    	System.out.println("Block Read Request Successful "+blockNumber);
   		    	readStrings.add(new String(rbres.getData().toByteArray()));
   		     }
   		     else
   		     {
   		    	 System.out.println("Reading Failed"+blockNumber);
   		     }
   		     
		 }
		 return readStrings;
		
	}
	
	public static String convertIntIp2String(int ip)
	{
		StringBuilder sb=new StringBuilder();
		int temp,maxValue=256*256*256;
		
		for(int i=0;i<=3;i++)
		{
			temp=ip/maxValue;
			sb.append(temp+".");
			ip=ip%maxValue;
			maxValue/=256;
		}
		sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	private static void write(String fileName,String data) 
	{
		System.out.println("\nIn Write\n");
		try
		{
			try
			{
				//open
				HDFS.OpenFileResponse ofresp=open(fileName,false);
				if(ofresp.getStatus()==0)
		        {
					System.out.println("Open Request Successful");
			    	int handle=ofresp.getHandle();
			    	byte[] dataValue=data.getBytes();
			    	
			    	for(int i=0;i<dataValue.length;i+=BlockSize)
			    	{
			    		//assign block request
			    		System.out.println("Assign Block Request");
			    		HDFS.AssignBlockRequest.Builder abreqBuilder=HDFS.AssignBlockRequest.newBuilder();
			    		abreqBuilder.setHandle(handle);
			    		
			    		byte[] abrespByte=TaskTracker.nameNodeRMI.assignBlock(abreqBuilder.build().toByteArray());
				    	HDFS.AssignBlockResponse abresp=HDFS.AssignBlockResponse.parseFrom(abrespByte);
				    		
				    	if(abresp.getStatus()==0)
					    {
				    		System.out.println("Assign Block Request Successful");
				    		String firstDNIP=convertIntIp2String(abresp.getNewBlock().getLocations(0).getIp());
				    			
			    			//datanode registry
			    			System.out.println("Looking For taskTracker_dataNode Registry");
		        			Registry datanodeRegistry = LocateRegistry.getRegistry(firstDNIP);
		        			String name1 = "taskTracker_dataNode";
		        		    IDataNode idatanodeRMI = (IDataNode) datanodeRegistry.lookup(name1);
			        		    
		        		    System.out.println("taskTracker_dataNode Lookup Successful");
			        		
		        		    //write
			        		System.out.println("Write Request At: "+firstDNIP);
			        		HDFS.WriteBlockRequest.Builder wbreqBuilder=HDFS.WriteBlockRequest.newBuilder();
			        		wbreqBuilder.setBlockInfo(abresp.getNewBlock());
			        		int ref;
			        		if(i+BlockSize<dataValue.length)
			        			ref=BlockSize;
			        		else
			        			ref=dataValue.length-i; //TOD Testing part TESTING
			        		wbreqBuilder.setData(ByteString.copyFrom(dataValue,i,ref));
			        		//TODO MAy give null values at end after data if <64KB
			        		
			        		byte[] wbrespByte=idatanodeRMI.writeBlock(wbreqBuilder.build().toByteArray());
					        HDFS.WriteBlockResponse wbresp=HDFS.WriteBlockResponse.parseFrom(wbrespByte);
					        if(wbresp.getStatus()==0)
			        		{
					        	System.out.println("Write Request Successful");
					        }
					    }
					}
			    	close(handle);
		    	}
			    else
		        {
					System.out.println("Open File Error");
		        }
			}
			catch (Exception e)
			{
				System.err.println("Exception In Writing");
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			System.err.println("Exception In Opening File Locally");
			e.printStackTrace();
		}
		
	}
	
	private static HDFS.OpenFileResponse open(String fileName,boolean forRead) throws Exception
	{
		 //open
		int openCount=3;
		HDFS.OpenFileRequest.Builder ofreqBuilder= HDFS.OpenFileRequest.newBuilder();
		ofreqBuilder.setFileName(fileName);
	    ofreqBuilder.setForRead(forRead);
	    
		System.out.println("Open Request");
		HDFS.OpenFileResponse ofresp;
		do
		{
  			 System.out.println("Open Request Attempt:"+openCount);
  			 byte[] ofrespByte=TaskTracker.nameNodeRMI.openFile(ofreqBuilder.build().toByteArray());
  			 ofresp=HDFS.OpenFileResponse.parseFrom(ofrespByte);
  			 if(ofresp.getStatus()==0)    
		    	return ofresp;
  			 openCount--;   
		}while(openCount!=0);
		
		return ofresp;
       
	}
	private static boolean close(int handle) throws Exception
	{
		 //close
	   	 System.out.println("Close Request");
		 HDFS.CloseFileRequest.Builder cfreqBuilder=HDFS.CloseFileRequest.newBuilder();
	   	 cfreqBuilder.setHandle(handle);
	   	 
	   	 int closeCount=3;
	   	
   		 do
	   	 {
   			 System.out.println("Close Request Attempt:"+closeCount);
	   		 byte[] cfrespByte=TaskTracker.nameNodeRMI.closeFile(cfreqBuilder.build().toByteArray());
	   		 HDFS.CloseFileResponse cfresp=HDFS.CloseFileResponse.parseFrom(cfrespByte);
	       	 if(cfresp.getStatus()==0)
	       	 {
	       		 System.out.println("Close Request Successful");
	       		 return true;
	       	 }
	       	 closeCount--;
	   	 }while(closeCount>0);
	   	
   		 System.out.println("Close Request Error");
	   	 return false;
   	
	}


}
