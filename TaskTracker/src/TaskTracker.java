import Server.*;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import ServerRMIInterface.IDataNode;
import ServerRMIInterface.IJobTracker;
import ServerRMIInterface.INameNode;

import com.google.protobuf.InvalidProtocolBufferException;

public class TaskTracker
{
	public static int uniqueId;
	public static IJobTracker jobTrackerRMI;
	public static IDataNode dataNodeRMI;
	public static INameNode nameNodeRMI;
	
	public TaskTracker(int id) {
	
		uniqueId=id;
		try
		{			
			
			new Thread(){
				public void run()
				{
					System.out.println("HeartBeat/BlockReport Thread");
					while(true)
					{
						try
						{
							heartBeat();
							sleep(2000);
						}
						catch(Exception e)
						{
							e.printStackTrace();
							System.exit(1);
						}
					
					}
						
				}


				
			}.start();
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void heartBeat() throws InvalidProtocolBufferException, RemoteException {
		MapReduce.HeartBeatRequest.Builder hbReq=TaskTrackerImplementations.heartbeatRequestGenerator();
		
		if(hbReq!=null)
		{
			MapReduce.HeartBeatResponse hbResp=MapReduce.HeartBeatResponse.parseFrom(jobTrackerRMI.heartBeat(hbReq.build().toByteArray()));
			if(hbResp.getStatus()==1)
			{
				System.out.println("Heart Beat Failed");
			}
			else
			{
				TaskTrackerImplementations.heartbeatResponseProcessor(hbResp);
			}
			
		}
		
	}
	
	
		
	public static void main(String[] args) 
	{
		

		if(args.length!=3)
		{
			System.err.println("Usage: java TaskTracker <JobTrackerIP> <Unique ID> <NameNodeIP>");
			System.exit(1);
		}
		try
		{
			if (System.getSecurityManager() == null) {
	            System.setSecurityManager(new SecurityManager());
	        }
			
			
			String name = "taskTracker_jobTracker";
            Registry registry = LocateRegistry.getRegistry(args[0]); //JobTracker IP
            jobTrackerRMI= (IJobTracker) registry.lookup(name);
            System.out.println("taskTracker_jobTracker lookup done");
                           
            String name2 = "taskTracker_nameNode";
            Registry registry2 = LocateRegistry.getRegistry(args[2]); //namenode IP
            nameNodeRMI= (INameNode) registry.lookup(name2);
            System.out.println("taskTracker_nameNode lookup done");
            
            
            TaskTracker tt=new TaskTracker(Integer.parseInt(args[1]));
        }
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		
	}

}
