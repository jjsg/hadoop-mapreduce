#!/bin/bash

if [ $# -eq 1 ] && [ $1 == "-c" ]
then
	mkdir bin >/dev/null 2>/dev/null
	javac -cp jars/protobuf-java-2.6.0.jar -d bin/ src/*/*.java
	javac -cp jars/protobuf-java-2.6.0.jar:bin -d bin/ src/*.java
elif [ $# -eq 4 ] && [ $1 == "-r" ]
then
	java -cp bin/:jars/protobuf-java-2.6.0.jar -Djava.security.policy=server.policy -Xmx2G TaskTracker $2 $3 $4		
else
 	echo "Usage: bash TaskTracker.sh -c //to compile"
	echo "       bash TaskTracker.sh -r <JobTrackerIP> <Unique ID> <NameNodeIP>" 			
fi


