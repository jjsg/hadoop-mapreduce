package Server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

import ServerRMIInterface.IDataNode;

import com.google.protobuf.ByteString;


public class DataNodeImplementations {

	public static HDFS.ReadBlockResponse.Builder readBlockImplementation(int block_number) {
		// TODO Auto-generated method stub
		
		try
		{
			System.out.println("Reading block number "+ block_number );
			HDFS.ReadBlockResponse.Builder readBlockResp=HDFS.ReadBlockResponse.newBuilder();
			try
			{
				RandomAccessFile raf=new RandomAccessFile(DataNode.path+"/"+block_number+".txt", "r");
				byte[] data=new byte[(int)raf.length()];
				
				raf.read(data);
				raf.close();
				System.out.println("Reading successful");
				readBlockResp.setData(ByteString.copyFrom(data));
				readBlockResp.setStatus(0);
				return readBlockResp;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("Error in reading block "+block_number);
				readBlockResp.setStatus(1);
				return readBlockResp;
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in read block response");
			e.printStackTrace();
		}
		
		return null;
	}

	public static HDFS.WriteBlockResponse.Builder writeBlockImplementation(int block_number,
			ByteString bs, List<Integer> dnlsIPs) {
		try
		{
			System.out.println("Writing block number "+block_number);
			HDFS.WriteBlockResponse.Builder writeBlockResp=HDFS.WriteBlockResponse.newBuilder();
			try
			{
				
				FileOutputStream fos=new FileOutputStream(DataNode.path+"/"+block_number);
				fos.write(bs.toByteArray());	
				fos.close();
				int t=dnlsIPs.indexOf(DataNode.myIp);
				dnlsIPs.remove(t);
				if(dnlsIPs.size()>0)
				{
					HDFS.WriteBlockRequest.Builder writereq=HDFS.WriteBlockRequest.newBuilder();
					writereq.setData(bs);
					HDFS.BlockLocations.Builder blockLocReq=HDFS.BlockLocations.newBuilder();
					blockLocReq.setBlockNumber(block_number);
					System.out.println("Other data nodes where it needs to be duplicated is");
					for(int i:dnlsIPs)
					{
						System.out.print(i+" ");
						HDFS.DataNodeLocation.Builder dataLoc=HDFS.DataNodeLocation.newBuilder();
						dataLoc.setIp(i);
						dataLoc.setPort(0);
						blockLocReq.addLocations(dataLoc);
					}
					writereq.setBlockInfo(blockLocReq);
					
					
					//RMI CODE
					System.out.println("Writing data to next node in list");
					String name = "DataNode_DataNode";
		            Registry registry = LocateRegistry.getRegistry(DataNode.InttoString(dnlsIPs.get(0)));
		            IDataNode dn= (IDataNode) registry.lookup(name);

					
					HDFS.WriteBlockResponse writeResp=HDFS.WriteBlockResponse.parseFrom(dn.writeBlock(writereq.build().toByteArray()));
					if(writeResp.getStatus()==1)
					{
						System.out.println("Error in writing in another datanode");
						writeBlockResp.setStatus(1);
						File f=new File(DataNode.path+"/"+block_number+".txt");
						if(f.exists())
							f.delete();
						return writeBlockResp;

					}
					System.out.println("Writing at other node is successful");
				}
				System.out.println("Writing done");
				
				writeBlockResp.setStatus(0);
				synchronized (DataNode.blockNumbers) {
					DataNode.blockNumbers.add(block_number);
				}
				try
				{
					DataNode.blockReport();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				return writeBlockResp;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("Error in write block"+block_number);
				writeBlockResp.setStatus(1);
				File f=new File(DataNode.path+"/"+block_number+".txt");
				if(f.exists())
					f.delete();
				return writeBlockResp;
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in write block response");
			e.printStackTrace();
		}
		
		return null;
	}


}
