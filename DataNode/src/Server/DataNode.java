package Server;

import java.io.File;
import java.net.InetAddress;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Server.HDFS.DataNodeLocation;
import ServerRMIInterface.IDataNode;
import ServerRMIInterface.INameNode;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

public class DataNode implements IDataNode
{
	public static Set<Integer> blockNumbers;
	public static INameNode nn;
	public static int uniqueId,myIp;
	static DataNodeLocation.Builder dnl;
	static String path,ip;
	static final int sleep1=2000;//600000ms,600sec,10mins
	
	public DataNode(String mip,int id) {
	
		try
		{
			dnl=HDFS.DataNodeLocation.newBuilder();
			ip=mip;
			myIp=StringtoInt(ip);
			dnl.setIp(myIp);
			dnl.setPort(0);
			blockNumbers=new HashSet<Integer>();
			uniqueId=id;
			
			File folder = new File(path);
			File[] listOfFiles = folder.listFiles();
			System.out.println("Files at this DataNode Are:");
			for (File file : listOfFiles) {
			    if (file.isFile()) {
			    		blockNumbers.add(Integer.parseInt(file.getName().split("\\.")[0]));	
			        System.out.println(file.getName());
			    }
			}
			
			System.out.println("Files Listed");
			

			
			new Thread(){
				public void run()
				{
					System.out.println("HeartBeat/BlockReport Thread");
					while(true)
					{
						try
						{
							heartBeat();
							sleep(2000);
							
							blockReport();
							sleep(sleep1);
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					
					}
						
				}


				
			}.start();
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void heartBeat() throws InvalidProtocolBufferException, RemoteException {
		HDFS.HeartBeatRequest.Builder heartReq=HDFS.HeartBeatRequest.newBuilder();
		heartReq.setId(uniqueId);
		
		HDFS.HeartBeatResponse heartResp=HDFS.HeartBeatResponse.parseFrom(nn.heartBeat(heartReq.build().toByteArray()));
		if(heartResp.getStatus()==1)
			System.out.println("Heart Beat Failed");
		
	}
	
	public static void blockReport() throws InvalidProtocolBufferException, RemoteException {
		HDFS.BlockReportRequest.Builder blockReportReq=HDFS.BlockReportRequest.newBuilder(); 
		blockReportReq.setId(uniqueId);
		blockReportReq.setLocation(dnl);
		synchronized (blockNumbers) {
			blockReportReq.addAllBlockNumbers(blockNumbers);
		}
		HDFS.BlockReportResponse blockResp=HDFS.BlockReportResponse.parseFrom(nn.blockReport(blockReportReq.build().toByteArray()));
		if(blockResp.getStatusList().get(0)==1)
			System.out.println("Block Report Failed");
		
	}

	
	@Override
	public byte[] readBlock(byte[] ReadBlockRequestData) throws RemoteException {
		
		int block_number;
		try {
			
			HDFS.ReadBlockRequest readBlockreq=HDFS.ReadBlockRequest.parseFrom(ReadBlockRequestData);
			block_number=readBlockreq.getBlockNumber();
			
			HDFS.ReadBlockResponse.Builder readBlockResp=DataNodeImplementations.readBlockImplementation(block_number);
			return readBlockResp.build().toByteArray();
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}

	@Override
	public byte[] writeBlock(byte[] WriteBlockRequestData)
			throws RemoteException {
		
		int block_number;
		List<Integer> dnlsIPs=new ArrayList<Integer>();
		ByteString bs;
		try {
			
			HDFS.WriteBlockRequest writeBlockreq=HDFS.WriteBlockRequest.parseFrom(WriteBlockRequestData);
			HDFS.BlockLocations blocLoc=writeBlockreq.getBlockInfo();
			block_number=blocLoc.getBlockNumber();
			for(DataNodeLocation dnl:blocLoc.getLocationsList())
			{
				dnlsIPs.add(dnl.getIp());
			}
			
			bs=writeBlockreq.getData(); //CHECK IT
			
			HDFS.WriteBlockResponse.Builder writeBlockResp=DataNodeImplementations.writeBlockImplementation(block_number,bs,dnlsIPs);
			return writeBlockResp.build().toByteArray();
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void main(String[] args) 
	{
		

		if(args.length!=4)
		{
			System.err.println("Usage: java DataNode <FS_DIR> <Namenode ip> <DataNode ip> <Unique ID>");
			System.exit(1);
		}
		try
		{

			path=args[0];	//dir where all the blocks will be stored as a file with name as blockNo.txt
			
			File f=new File(path);
			if(!f.exists())
			{
				System.err.println("Usage: java DataNode <FS_DIR> <Namenode ip> <Unique ID>");
				System.exit(1);
			}
			
			
			if (System.getSecurityManager() == null) {
	            System.setSecurityManager(new SecurityManager());
	        }
	        
	        String name = "DataNode_NameNode";
            Registry registry = LocateRegistry.getRegistry(args[1]); //Namenode IP
            nn= (INameNode) registry.lookup(name);
            System.out.println("DataNode_NameNode lookup done");
			
			try
				{
					LocateRegistry.createRegistry(1099);
				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("Cant create registry");
				}
			
			
            
            String name2 = "Client_DataNode";
            IDataNode engine = new DataNode(args[2],Integer.parseInt(args[3])); //UniqueID
            IDataNode stub =(IDataNode) UnicastRemoteObject.exportObject(engine, 0);
            Registry myregistry = LocateRegistry.getRegistry();
           
            myregistry.rebind(name2, stub);
            System.out.println("Client_DataNode bound");
            
            
            String name3 = "DataNode_DataNode";
            //IDataNode stub2 =(IDataNode) UnicastRemoteObject.exportObject(engine, 0);
           // Registry myregistry = LocateRegistry.getRegistry();
            myregistry.rebind(name3, stub);
            System.out.println("DataNode_DataNode bound");
            
            String name4="taskTracker_dataNode";
            myregistry.rebind(name4, stub);
            System.out.println("taskTracker_dataNode bound");
            
            
            System.out.println(ip+"  "+myIp);
            
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	
	public static int StringtoInt(String ip)
	{
		int res=0,temp=1;
		String octets[]=ip.split("\\.");
		for(int i=3;i>=0;i--)
		{
			res+=Integer.parseInt(octets[i])*temp;
			temp=temp*256;
		}
		
		return res;
	}
	
	public static String InttoString(int ip)
	{
		StringBuilder sb=new StringBuilder();
		int temp,maxValue=256*256*256;
		
		for(int i=0;i<=3;i++)
		{
			temp=ip/maxValue;
			sb.append(temp+".");
			ip=ip%maxValue;
			maxValue/=256;
		}
		sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}

}
