if [ $# -eq 0 ]
then
 	echo "Usage: bash DataNode.sh -c //to compile"
	echo "       bash DataNode.sh -r <Dir to Store data> <NameNode IP> <My ip> <unique_id>" 			
elif [ $1 == "-c" ]
then
	javac -cp jars/protobuf-java-2.6.0.jar -d bin/ src/*/*.java
elif [ $1 == "-r" ]
then
	 java -Djava.rmi.server.hostname=$4 -Djava.security.policy=server.policy -cp bin/:jars/protobuf-java-2.6.0.jar Server/DataNode $2 $3 $4 $5 
fi
