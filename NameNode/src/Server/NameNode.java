package Server;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import Server.HDFS.DataNodeLocation;
import ServerRMIInterface.INameNode;

import com.google.protobuf.InvalidProtocolBufferException;



public class NameNode implements INameNode
{
	public static Map<Integer,Set<Integer> >dataNodeDetails; //Block No- Array of Data Node Location
	public static Map<String,ArrayList<Integer> > fileDetails; //File Name-Array of blocks
	public static Map<String,ArrayList<Integer> > tempFileDetails; //File Name-Array of blocks
	public static Map<Integer,String> readfileInfo; //Handle-File Name
	public static Map<Integer,String> writefileInfo; //Handle-File Name
	public static Map<Integer,Integer> dataNodeIdIp;  //DataNode ID-IP mapping
	public static Map<Integer,Long> dataNodeLastBeat;  //ID-Timestamp Last time when each data node called heartbeat
	public static Integer handleCount=0;
	public static Integer blockNumbers=1;
	public static int replicationFactor=2; //Indicate in how many datanodes block needs to be replicated
	public static int sleep1=300000;//5min
	public static int allowedDifference=660000;//11min
	BufferedReader br;
	
	String line;
	String[] linedetails;
	ArrayList<Integer> ref;
	
	public NameNode(final String fileDetailsPath) {
		dataNodeDetails=new HashMap<Integer, Set<Integer>>();
		readfileInfo=new HashMap<Integer,String>();
		writefileInfo=new HashMap<Integer,String>();
		dataNodeIdIp=new HashMap<Integer, Integer>();
		dataNodeLastBeat=new HashMap<Integer, Long>();
		fileDetails=new HashMap<String, ArrayList<Integer>>();
		tempFileDetails=new HashMap<String, ArrayList<Integer>>();
		try 
		{
			br=new BufferedReader(new FileReader(fileDetailsPath));
			line=br.readLine();
			
			int max=1;
			System.out.println("Reading file");
			//Each line in the file will be of format: Name-blockno1,blockno2,....
			while(line!=null)
			{
				linedetails=line.split("-");
				ref=new ArrayList<Integer>();
				for(int i=1;i<linedetails.length;i++)
				{
					int temp=Integer.parseInt(linedetails[i]);
					if(dataNodeDetails.containsKey(temp))
					{
						
					}
					else
					{
						dataNodeDetails.put(temp,new HashSet<Integer>());
					}
					if(temp>max)
						max=temp;
					ref.add(temp);
				}
				fileDetails.put(linedetails[0],ref);
				line=br.readLine();
			}
			br.close();
			System.out.println("Reading file done");
			blockNumbers=max;
			
			new Thread(){
				public void run()
				{
					while(true)
					{
						try {
							sleep(sleep1);
							PrintWriter pw=new PrintWriter(fileDetailsPath);	
							StringBuilder sb=new StringBuilder();
							synchronized (fileDetails) {
								System.out.println("Files written are:");
								for(String s:fileDetails.keySet())
								{
									System.out.print(s+"   ");
									sb.append(s);
									for(Integer i : fileDetails.get(s))
									{
										sb.append("-"+i);
									}
									sb.append("\n");
								}
								System.out.println();

							}
							pw.write(sb.toString());
							pw.close();
						}
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
				
			}.start();
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	

	@Override
	public byte[] openFile(byte[] OpenFileRequestData) {
		String fileName;
		boolean forRead;
		try {
			
			HDFS.OpenFileRequest ofreq=HDFS.OpenFileRequest.parseFrom(OpenFileRequestData);
			fileName=ofreq.getFileName();
			forRead=ofreq.getForRead();
			
			HDFS.OpenFileResponse.Builder ofrespBuild=NameNodeImplementations.openImplementation(fileName,forRead);
			return ofrespBuild.build().toByteArray();
		}
		catch (InvalidProtocolBufferException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	

	@Override
	public byte[] closeFile(byte[] CloseFileRequestData) {
		int handle;
		try {
			
			HDFS.CloseFileRequest closereq=HDFS.CloseFileRequest.parseFrom(CloseFileRequestData);
			handle=closereq.getHandle();
			
			HDFS.CloseFileResponse.Builder closerespBuild=NameNodeImplementations.closeImplementation(handle);
			return closerespBuild.build().toByteArray();
		}
		catch (InvalidProtocolBufferException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public byte[] getBlockLocations(byte[] BlockLocationRequestData) {
		
		try {
			
			HDFS.BlockLocationRequest blockLocreq=HDFS.BlockLocationRequest.parseFrom(BlockLocationRequestData);
			List<Integer> ref=blockLocreq.getBlockNumsList();
			
			HDFS.BlockLocationResponse.Builder blockLocResp=NameNodeImplementations.blockLocationImplementation(ref);
			return blockLocResp.build().toByteArray();
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}

	@Override
	public byte[] assignBlock(byte[] AssignBlockRequestData) {
		int handle;
		try {
			
			HDFS.AssignBlockRequest assignreq=HDFS.AssignBlockRequest.parseFrom(AssignBlockRequestData);
			handle=assignreq.getHandle();
			
			HDFS.AssignBlockResponse.Builder assignResp=NameNodeImplementations.assignBlockImplementation(handle);
			return assignResp.build().toByteArray();
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public byte[] list(byte[] ListFilesRequestData) {
		try {
			
			HDFS.ListFilesRequest listFileReq=HDFS.ListFilesRequest.parseFrom(ListFilesRequestData);
			
			HDFS.ListFilesResponse.Builder listrespBuild=NameNodeImplementations.listImplementation();
			return listrespBuild.build().toByteArray();
		}
		catch (InvalidProtocolBufferException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public byte[] blockReport(byte[] BlockReportRequestData) {
		int id;
		List<Integer> blockNos;
		DataNodeLocation dnl;
		try {
			
			HDFS.BlockReportRequest blockReq=HDFS.BlockReportRequest.parseFrom(BlockReportRequestData);
			id=blockReq.getId();
			blockNos=blockReq.getBlockNumbersList();
			dnl=blockReq.getLocation();
			
			HDFS.BlockReportResponse.Builder blockresp=NameNodeImplementations.blockReportImplementation(id,blockNos,dnl );
			
			return blockresp.build().toByteArray();
		}
		catch (InvalidProtocolBufferException e)
		{
			e.printStackTrace();
		}
		
		return null;	
	}

	@Override
	public byte[] heartBeat(byte[] HeartBeatRequestData) {
		int id;
		try {
			
			HDFS.HeartBeatRequest heartReq=HDFS.HeartBeatRequest.parseFrom(HeartBeatRequestData);
			id=heartReq.getId();
			
			HDFS.HeartBeatResponse.Builder heartresp=NameNodeImplementations.heartBeatImplementation(id);
			
			return heartresp.build().toByteArray();
		}
		catch (InvalidProtocolBufferException e)
		{
			e.printStackTrace();
		}
		

		return null;
	}

	
	public static void main(String[] args) {
		try
		{
			INameNode nn=new NameNode(args[0]);
			INameNode stub=(INameNode) UnicastRemoteObject.exportObject(nn, 0);
			
				try
				{
					LocateRegistry.createRegistry(1099);
				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("Cant create registry");
				}
			
			Registry reg=LocateRegistry.getRegistry();
			
			String name="Client_NameNode";
			reg.rebind(name, stub);
			System.out.println("Client_NameNode Bound");
			
			String name2="DataNode_NameNode";
			reg.rebind(name2, stub);
			System.out.println("DataNode_NameNode Bound");
			
			String name3="taskTracker_nameNode";
			reg.rebind(name3, stub);
			System.out.println("taskTracker_nameNode Bound");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}
}
