package Server;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;


import Server.HDFS.DataNodeLocation;

public class NameNodeImplementations {

	
	public static HDFS.OpenFileResponse.Builder openImplementation(String fileName, boolean forRead)
	{
		HDFS.OpenFileResponse.Builder ofsbuilder=null;
		try
		{
			ofsbuilder=HDFS.OpenFileResponse.newBuilder();
			ArrayList<Integer> ref;
			if(forRead)
			{
				synchronized (NameNode.fileDetails) {
					if(NameNode.fileDetails.containsKey(fileName))
					{
						ref=NameNode.fileDetails.get(fileName);
					}
					else
					{
						System.out.println("File not present");
						ofsbuilder.setStatus(1);
						return ofsbuilder;
					
					}
				}
				
				System.out.println("Opening file "+fileName+" for reading");
				synchronized (NameNode.writefileInfo) {
					if(NameNode.writefileInfo.containsValue(fileName))
					{
						System.out.println("File is being written");
						ofsbuilder.setStatus(1);
						return ofsbuilder;
					}
				}
		
				synchronized (NameNode.handleCount) {
					NameNode.handleCount++;
					ofsbuilder.setHandle(NameNode.handleCount);
					NameNode.readfileInfo.put(NameNode.handleCount,fileName);
				}
				System.out.println("Handle Assigned"+ofsbuilder.getHandle());
			
				for(Integer i:ref)
				{
					ofsbuilder.addBlockNums(i);
				}
				ofsbuilder.setStatus(0);
				return ofsbuilder;
			}
			else
			{
				//WRITE
				/*synchronized (NameNode.writefileInfo) {
					
					if(NameNode.writefileInfo.containsValue(fileName))
					{
						ofsbuilder.setStatus(1);
						return ofsbuilder;
					}
				}
				
				synchronized (NameNode.handleCount) {
					if(NameNode.readfileInfo.containsValue(fileName))
					{
							ofsbuilder.setStatus(1);
							return ofsbuilder;
					}
					NameNode.handleCount++;
					ofsbuilder.setHandle(NameNode.handleCount);
					synchronized (NameNode.writefileInfo) {
						NameNode.writefileInfo.put(NameNode.handleCount,fileName);
					}
				}
				*/
				System.out.println("Opening file "+fileName+" for writing");
				synchronized (NameNode.fileDetails) {
					if(NameNode.fileDetails.containsKey(fileName))
					{
						System.out.println("File already existing");
						ofsbuilder.setStatus(1);
						return ofsbuilder;
					}
					else
					{
						NameNode.tempFileDetails.put(fileName, new ArrayList<Integer>());
						
					}
				}
				
				synchronized (NameNode.handleCount) {
					NameNode.handleCount++;
					ofsbuilder.setHandle(NameNode.handleCount);
					synchronized (NameNode.writefileInfo) {
						NameNode.writefileInfo.put(NameNode.handleCount,fileName);
					}
				}
				System.out.println("Handle Assigned"+ofsbuilder.getHandle());
				
				ofsbuilder.setStatus(0);
				return ofsbuilder;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			synchronized (NameNode.handleCount) {
				if(NameNode.readfileInfo.containsKey(ofsbuilder.getHandle()))
						NameNode.readfileInfo.remove(ofsbuilder.getHandle());
			}
			
			synchronized (NameNode.writefileInfo) {
				if(NameNode.writefileInfo.containsKey(ofsbuilder.getHandle()))
					NameNode.writefileInfo.remove(ofsbuilder.getHandle());
			}
			System.out.println("Error in opening");
			ofsbuilder.setStatus(1);
			return ofsbuilder;
		}
	}

	public static HDFS.BlockLocationResponse.Builder blockLocationImplementation(List<Integer> ref) {
		// TODO Auto-generated method stub
		
		try
		{
			HDFS.BlockLocationResponse.Builder blockLocResp=HDFS.BlockLocationResponse.newBuilder();
			try
			{
				for(Integer i:ref)
				{
					HDFS.BlockLocations.Builder blockLoc=HDFS.BlockLocations.newBuilder();
					
					blockLoc.setBlockNumber(i);
					synchronized (NameNode.dataNodeDetails) {
					System.out.println("Block Number "+i+" is at following locations:");
					Set<Integer> toBeRemovedInfo=new HashSet<Integer>();
					
					if(NameNode.dataNodeDetails.get(i).size()==0)
					{
						System.out.println("No block locations");
						blockLocResp.setStatus(1);
						return blockLocResp;
						
					}
					
						for(Integer dnl:NameNode.dataNodeDetails.get(i))
						{
							System.out.print(dnl+"  ");
							DataNodeLocation.Builder dnlbuild=DataNodeLocation.newBuilder();
							Long currenttime=System.currentTimeMillis();
							Long temp;
							synchronized (NameNode.dataNodeLastBeat) {
								temp=NameNode.dataNodeLastBeat.get(dnl);
							}
							if(currenttime-temp<=NameNode.allowedDifference)
							{
								synchronized (NameNode.dataNodeIdIp) {
									dnlbuild.setIp(NameNode.dataNodeIdIp.get(dnl));
								}
								dnlbuild.setPort(0);
								blockLoc.addLocations(dnlbuild);
							}
							else
								toBeRemovedInfo.add(dnl);
						}
						
						for(Integer dnl:toBeRemovedInfo)
						{
							NameNode.dataNodeDetails.get(i).remove(dnl);
						}
						System.out.println();

					}
					blockLocResp.addBlockLocations(blockLoc);
				}
				blockLocResp.setStatus(0);
				return blockLocResp;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("Error in getting block locations");
				blockLocResp.setStatus(1);
				return blockLocResp;
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in creating block location response");
			e.printStackTrace();
		}
		
		return null;
	}

	public static HDFS.CloseFileResponse.Builder closeImplementation(int handle) {
		
		try
		{
			HDFS.CloseFileResponse.Builder closeResp=HDFS.CloseFileResponse.newBuilder();
			try
			{
				String fileName;
				System.out.println("File having handle "+handle+" is getting closed");
				synchronized (NameNode.handleCount) {
					
					if(NameNode.readfileInfo.containsKey(handle))
					{
						NameNode.readfileInfo.remove(handle);
						closeResp.setStatus(0);
						return closeResp;
					}
					
				}
//				if(!flag)
	//			{
					//Not written if inside synchronized since in case not there in writefile means some error
					synchronized (NameNode.writefileInfo) {
							fileName=NameNode.writefileInfo.get(handle);
							NameNode.writefileInfo.remove(handle);
							synchronized (NameNode.fileDetails) {
								NameNode.fileDetails.put(fileName,NameNode.tempFileDetails.get(fileName));
								NameNode.tempFileDetails.remove(fileName);
							}
					}
		//		}
				closeResp.setStatus(0);
				return closeResp;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("Error in closing file");
				closeResp.setStatus(1);
				return closeResp;
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in creating closing response");
			e.printStackTrace();
		}
		
		return null;
	}

	public static HDFS.ListFilesResponse.Builder listImplementation() {
		
		try
		{
			HDFS.ListFilesResponse.Builder listResp=HDFS.ListFilesResponse.newBuilder();
			try
			{
				Set<String> fileNames;
				synchronized (NameNode.fileDetails) {
					fileNames=NameNode.fileDetails.keySet();
				}
				System.out.println("Files at server are:");
				for(String s:fileNames)
				{
					System.out.print(s+" ");
					listResp.addFileNames(s);
				}
				System.out.println();
				listResp.setStatus(0);
				return listResp;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("Error in listing files");
				listResp.setStatus(1);
				return listResp;
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in response of list");
			e.printStackTrace();
		}
		
		return null;
	}

	public static HDFS.BlockReportResponse.Builder blockReportImplementation(int id,
			List<Integer> blockNos, DataNodeLocation dnl) {
		
		try
		{
			HDFS.BlockReportResponse.Builder blockResp=HDFS.BlockReportResponse.newBuilder();
			try
			{
				synchronized (NameNode.dataNodeIdIp) {
					
					NameNode.dataNodeIdIp.put(id,dnl.getIp());
				}
				
				if(blockNos.size()==0)
				{
					blockResp.addStatus(0);
					return blockResp;
				}
				for(Integer i:blockNos)
				{
					synchronized (NameNode.dataNodeDetails) {
						NameNode.dataNodeDetails.get(i).add(id);
					}
					blockResp.addStatus(0);
				}
				return blockResp;
			}
			catch(Exception e)
			{
//				e.printStackTrace();
				System.out.println("Error in block report: DataNode has unknown files");
				blockResp.clearStatus().addStatus(1);
				return blockResp;
			}
		}
		catch(Exception e)
		{
			System.out.println("Erorr in block report response");
			e.printStackTrace();
		}
		
		return null;
	}

	public static HDFS.HeartBeatResponse.Builder heartBeatImplementation(int id) {
		
		try
		{
			HDFS.HeartBeatResponse.Builder heartResp=HDFS.HeartBeatResponse.newBuilder();
			try
			{
				Long time=System.currentTimeMillis();
				synchronized (NameNode.dataNodeLastBeat) {
					
					NameNode.dataNodeLastBeat.put(id,time);
				}
				heartResp.setStatus(0);
				return heartResp;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("Error in heart beat");
				heartResp.setStatus(1);
				return heartResp;
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in heart beat response");
			e.printStackTrace();
		}
		
		
		return null;
	}

	public static HDFS.AssignBlockResponse.Builder assignBlockImplementation(int handle) {
		
		try
		{
			String fileName;
			int blockNo;
			Long currentTime;
			Set<Integer> dnlIds,temp;
			Random randomgen;
			HDFS.AssignBlockResponse.Builder assignResp=HDFS.AssignBlockResponse.newBuilder();
			try
			{
				temp=new HashSet<Integer>();
				dnlIds=new HashSet<Integer>();
				currentTime=System.currentTimeMillis();
				HDFS.BlockLocations.Builder blockLoc=HDFS.BlockLocations.newBuilder();
				
				synchronized (NameNode.dataNodeLastBeat) {
					
					for(int id:NameNode.dataNodeLastBeat.keySet())
					{
						if(currentTime-NameNode.dataNodeLastBeat.get(id)<=NameNode.allowedDifference)
						{
							temp.add(id);
						}
					}
				}
				if(temp.size()==0)
				{
					System.out.println("No data Nodes are up");
					assignResp.setStatus(1);
					return assignResp;
				}
				else if(temp.size()<=NameNode.replicationFactor)
				{
					dnlIds=temp;
				}
				else
				{
					randomgen=new Random();
					while(dnlIds.size()!=NameNode.replicationFactor)
						dnlIds.add(randomgen.nextInt(temp.size()));
				}
				
				synchronized (NameNode.blockNumbers) {
					NameNode.blockNumbers++;
					blockNo=NameNode.blockNumbers;
					
				}

				System.out.println("New block number being assigned is "+ blockNo);
				System.out.println("DataNodes where its replicated are:");
				synchronized (NameNode.dataNodeDetails) {
					NameNode.dataNodeDetails.put(blockNo,new HashSet<Integer>());
				}
				for(int id:dnlIds)
				{
					System.out.print(id+" ");
					HDFS.DataNodeLocation.Builder dnlLoc=HDFS.DataNodeLocation.newBuilder();
					synchronized (NameNode.dataNodeIdIp) {
						System.out.print(NameNode.dataNodeIdIp.get(id));
						dnlLoc.setIp(NameNode.dataNodeIdIp.get(id));
						dnlLoc.setPort(0);
					}
					
					
					blockLoc.addLocations(dnlLoc);
				}
				System.out.println();
				synchronized (NameNode.writefileInfo) {
					fileName=NameNode.writefileInfo.get(handle);
				}
				
				
				synchronized (NameNode.fileDetails) {
					NameNode.tempFileDetails.get(fileName).add(blockNo);
				}
				
				blockLoc.setBlockNumber(blockNo);
				
				assignResp.setNewBlock(blockLoc);
				assignResp.setStatus(0);
				return assignResp;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("Error in assigning block");
				assignResp.setStatus(1);
				return assignResp;
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in assign block response");
			e.printStackTrace();
		}
		
		return null;
	}
}
