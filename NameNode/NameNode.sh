if [ $# -eq 1 ] && [ $1 == "-c" ]
then
	mkdir bin >/dev/null 2>/dev/null
	javac -cp jars/protobuf-java-2.6.0.jar -d bin/ src/*/*.java
elif [ $# -eq 3 ] && [ $1 == "-r" ]
then
	 java -Djava.rmi.server.hostname=$2 -Djava.security.policy=server.policy -cp bin/:jars/protobuf-java-2.6.0.jar Server/NameNode $3 
else
 	echo "Usage: bash NameNode.sh -c //to compile"
	echo "       bash NameNode.sh -r <M/C IP> <Path to store file details>" 			
fi
