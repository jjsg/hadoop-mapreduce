package Client;
import java.io.File;
import java.io.RandomAccessFile;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import ServerRMIInterface.IDataNode;
import ServerRMIInterface.IJobTracker;
import ServerRMIInterface.INameNode;

import com.google.protobuf.ByteString;

public class Client
{
	private static String jobTrackerIp="";
	private static String namenodeIP="";
	private static Registry namenodeRegistry;
	private static String name = "Client_NameNode";
    private static INameNode inamenodeRMI ;
    private static Registry jobTrackerRegistry;
	private static String name2 = "jobClient_jobTracker";
    private static IJobTracker ijobTrackerRMI ;
    private static int BlockSize=16*1024*1024; //16MB
	public static void main(String[] args)
	{
		if(args.length!=1)
		{
			System.err.println("Usage: java -Djava.security.policy=client.policy Client <Namenode ip>");
			System.exit(1);
		}
		
		namenodeIP=args[0];
		try
		{
		
		RandomAccessFile raf=new RandomAccessFile("conf","r");
		jobTrackerIp=raf.readLine();
		raf.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.err.println("Error in reading file. Exiting");
			System.exit(1);
		}
		
		
		if (System.getSecurityManager() == null)
		{
	           System.setSecurityManager(new SecurityManager());
	    }
		
		System.out.println("Security Manager Set");
		
		try
		{
			System.out.println("Looking For Client_NameNode Registry");
			namenodeRegistry = LocateRegistry.getRegistry(namenodeIP);
			inamenodeRMI = (INameNode) namenodeRegistry.lookup(name);
		}
		catch(Exception e)
		{
			System.err.println("Exception In Locating Registry");
			e.printStackTrace();
			System.exit(1);
		}
		
		System.out.println("Client_NameNode Lookup Done");
		
		try
		{
			System.out.println("Looking For jobClient_jobTracker Registry on IP"+jobTrackerIp);
			jobTrackerRegistry = LocateRegistry.getRegistry(jobTrackerIp);
			ijobTrackerRMI= (IJobTracker) jobTrackerRegistry.lookup(name2);
		}
		catch(Exception e)
		{
			System.err.println("Exception In Locating Registry");
			e.printStackTrace();
			System.exit(1);
		}
		
		System.out.println("jobClient_jobTracker Lookup Done");
		
		Scanner sc=new Scanner(System.in);
		while(true)
		{
			System.out.println("\nMenu:");
			System.out.print("Enter r/w/l/m/x to read/write/list/map-reduce/exit:\t");
			String choice=sc.nextLine();
			try{
				if(choice.charAt(0)=='r'||choice.charAt(0)=='R')
				{
					System.out.print("Enter the filename to read:\t");
					String fn=sc.nextLine();
					read(fn);
					
				}
				else if(choice.charAt(0)=='l'||choice.charAt(0)=='L')
				{
					list();
				}
				else if(choice.charAt(0)=='w'||choice.charAt(0)=='W')
				{
					System.out.print("Enter the path of file to write:\t");
					String fn=sc.nextLine();
					write(fn);
				}
				else if(choice.charAt(0)=='x'||choice.charAt(0)=='X')
				{
					sc.close();
					System.exit(0);
				}
				else if(choice.charAt(0)=='m'||choice.charAt(0)=='M')
				{
					//<mapName> <reducerName> <inputFile in HDFS> <outputFile in HDFS> <numReducers>
					System.out.println("Enter <mapName> <reducerName> <inputFile in HDFS> <outputFile in HDFS> <numReducers>");
					String mapName=sc.next();
					String reducerName=sc.next();
					String inputFile=sc.next();
					String outputFile=sc.next();
					int noReducers=Integer.parseInt((sc.nextLine()).trim());
					mapReduce(mapName,reducerName,inputFile,outputFile,noReducers);
				}
				else 
				{
					System.out.println("Inavlid Arg");
				}
			}
			catch(Exception e)
			{
				System.out.println("Inavlid Arg");
			}
		}
		
	
		
	}
	
	private static void mapReduce(String mapName, String reducerName,
			String inputFile, String outputFile, int noReducers) {
		

		MapReduce.JobSubmitRequest.Builder jSubReq=MapReduce.JobSubmitRequest.newBuilder();
		
		jSubReq.setMapName(mapName);
		jSubReq.setReducerName(reducerName);
		jSubReq.setInputFile(inputFile);
		jSubReq.setOutputFile(outputFile);
		jSubReq.setNumReduceTasks(noReducers);
		
		try 
		{
			byte[] jSubRespByte=ijobTrackerRMI.jobSubmit(jSubReq.build().toByteArray());
			MapReduce.JobSubmitResponse jSubResp=MapReduce.JobSubmitResponse.parseFrom(jSubRespByte);
			
			
			if(jSubResp.getStatus()==1)
			{
				System.out.println("Job Submit Request Failed");
				System.exit(1);
			}
			else
			{
				int jobId=jSubResp.getJobId();
				while(true)
				{
					Thread.sleep(3000);
					MapReduce.JobStatusRequest.Builder jStatReq=MapReduce.JobStatusRequest.newBuilder();
					jStatReq.setJobId(jobId);
					
					byte[] jStatRespByte=ijobTrackerRMI.getJobStatus(jStatReq.build().toByteArray());
					MapReduce.JobStatusResponse jStatResp=MapReduce.JobStatusResponse.parseFrom(jStatRespByte);
							//Print Report on console after every 30sec
					if(jStatResp.getStatus()!=1)
					{
						if(jStatResp.getJobDone())
						{
							System.out.println("Job Done...................");
							break;
						}
						else //if(count==10)
						{
							int totalMap=jStatResp.getTotalMapTasks();
							int startedMap=jStatResp.getNumMapTasksStarted();
							int totalReduce=jStatResp.getTotalReduceTasks();
							int startedReduce=jStatResp.getNumReduceTasksStarted();
							
							System.out.println("Progress:");
							System.out.println("Map Task="+((float)startedMap*100)/totalMap+"%");
							System.out.println("Reduce Task="+((float)startedReduce*100)/totalReduce+"%");
						}
					}
					else
					{
						System.out.println("Job Failed");
						return;
					}
				}
			}
			
		} 
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		

	}

	private static void write(String fileName) 
	{
		System.out.println("\nIn Write\n");
		RandomAccessFile raf=null;
		try
		{
			raf=new RandomAccessFile(fileName, "r");
			try
			{
				//open
				File f=new File(fileName);
				HDFS.OpenFileResponse ofresp=open(f.getName(),false);
				if(ofresp.getStatus()==0)
		        {
					System.out.println("Open Request Successful");
			    	int handle=ofresp.getHandle();
			    	int n=-1;
			    	byte[] data;
			    	
			    	data=new byte[BlockSize];
		    		n=raf.read(data);
		    		System.out.println("Block Read Locally");
			    	boolean writeSuccessful=true;
		    		while(n!=-1)
			    	{
			    		//assign block request
			    		System.out.println("Assign Block Request");
			    		HDFS.AssignBlockRequest.Builder abreqBuilder=HDFS.AssignBlockRequest.newBuilder();
			    		abreqBuilder.setHandle(handle);
			    		
			    		int abCount=3;
			    		do
			    		{
			    			System.out.println("Assign Block Request Attempt "+abCount);
			    		
				    		byte[] abrespByte=inamenodeRMI.assignBlock(abreqBuilder.build().toByteArray());
				    		HDFS.AssignBlockResponse abresp=HDFS.AssignBlockResponse.parseFrom(abrespByte);
				    		
				    		if(abresp.getStatus()==0)
					        {
				    			System.out.println("Assign Block Request Successful");
				    			String firstDNIP=convertIntIp2String(abresp.getNewBlock().getLocations(0).getIp());
				    			
				    			//datanode registry
				    			System.out.println("Looking For Client_DataNode Registry");
			        			Registry datanodeRegistry = LocateRegistry.getRegistry(firstDNIP);
			        			String name1 = "Client_DataNode";
			        		    IDataNode idatanodeRMI = (IDataNode) datanodeRegistry.lookup(name1);
			        		    
			        		    System.out.println("Client_DataNode Lookup Successful");
			        		    
			        		    //write
			        		    System.out.println("Write Request At: "+firstDNIP);
			        		    HDFS.WriteBlockRequest.Builder wbreqBuilder=HDFS.WriteBlockRequest.newBuilder();
			        		    wbreqBuilder.setBlockInfo(abresp.getNewBlock());
			        		    wbreqBuilder.setData(ByteString.copyFrom(data,0,n));
			        		    
			        		    int writeCount=3;
			        		   
			        		   	 do
			        		   	 {
			        		   		 System.out.println("Write Request Attempt"+writeCount);
			        		   		 byte[] wbrespByte=idatanodeRMI.writeBlock(wbreqBuilder.build().toByteArray());
					        		 HDFS.WriteBlockResponse wbresp=HDFS.WriteBlockResponse.parseFrom(wbrespByte);
					        		 if(wbresp.getStatus()==0)
			        		       	 {
					        			 System.out.println("Write Request Successful");
					        			 break;
			        		       	 }
					        		 writeCount--;
			        		   	 }while(writeCount>0);
			        		   	 
			        		   	 if(writeCount==0)
			        		   	 {
			        		   		 System.out.println("Write Block Error");
			        		   		 writeSuccessful=false;
			        		   	 }
			        		   	 break; 
					        }
				    		abCount--;
			        	}
			    		while(abCount!=0);
			    		if(abCount==0)	
			    		{
			    			System.out.println("Assign Block Error");
			    			writeSuccessful=false;
			    			break;
			    		}
			    		
			    		data=new byte[BlockSize];
			    		n=raf.read(data);
			    		System.out.println("Block Read Locally");
			    	}
			    	
		    		//close
		        	 if(writeSuccessful)
		        	 {
		        		 if(close(handle))
		        		 {
		        			 System.out.println("\nDone Writing Successfully\n");
		        		 }
		        		 else
		        		 {
		        			 System.out.println("\nDone Writing But Could Not Close File\n");
		        		 }
		        	}
		        	 else
		        	 {
	        			 System.out.println("\nWriting Failed\n");
		        	 }
		        	
			    	
		        }
			    else
		        {
					System.out.println("Open File Error");
		        }
			}
			catch (Exception e)
			{
				try{
					if(raf!=null)
					{
						raf.close();
					}
				}catch(Exception e1)
				{
					
				}
				System.err.println("Exception In Writing");
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			try{
				if(raf!=null)
				{
					raf.close();
				}
			}catch(Exception e1)
			{
				
			}
			System.err.println("Exception In Opening File Locally");
			e.printStackTrace();
		}
		
	}

	private static void read(String fileName)
	{
		 try 
		 {
			 System.out.println("\nIn Read\n");
			 //open
			 HDFS.OpenFileResponse ofresp=open(fileName,true);
	        
	         if(ofresp.getStatus()==0)
	         {
	        	
	        	 System.out.println("Open Request Successful");
	        	 int handle=ofresp.getHandle();
	        	
	        	 //block location request
	        	 System.out.println("Block Location Request");
		         HDFS.BlockLocationRequest.Builder blreqBuilder= HDFS.BlockLocationRequest.newBuilder();
	        	 blreqBuilder.addAllBlockNums(ofresp.getBlockNumsList());
	        	 int blCount=3;
	        	 do
	        	 {
		        	 System.out.println("Block Location Attempt"+blCount);

		        	 byte[] blrespByte=inamenodeRMI.getBlockLocations(blreqBuilder.build().toByteArray());
		        	 HDFS.BlockLocationResponse blresp=HDFS.BlockLocationResponse.parseFrom(blrespByte);
		        	 
		        	 if(blresp.getStatus()==0)
		        	 {
		        		 System.out.println("Block Location Request Successful");
		        		 List<HDFS.DataNodeLocation> availableDatanodeIP;
		        		 HDFS.DataNodeLocation dnl;
		        		 HDFS.ReadBlockRequest.Builder rbreqBuilder;
		        		 HDFS.ReadBlockResponse rbres;
		        		 byte[] rbresByte;
		        		 String datanodeIP;
		        		 int blockNumber;
		        		 int rand;
		        		 Random randomGenerator = new Random();
		        		 for(HDFS.BlockLocations bl:blresp.getBlockLocationsList())
		        		 {
		        			 blockNumber=bl.getBlockNumber();
		        			 availableDatanodeIP=bl.getLocationsList();
		        			 if(availableDatanodeIP.size()==0)
		        			 {
		        				 System.out.println("No Available Node to read, Block Read Error "+blockNumber);
		        				 return;
		        			 }
		        			 
		        			 int readCount=3;
		        			 do
		        			 {
			        			 rand=randomGenerator.nextInt(availableDatanodeIP.size());
			        			 dnl=bl.getLocations(rand);
			        			 datanodeIP=convertIntIp2String(dnl.getIp());
			        			 
			        			 //datanode registry
			        			 System.out.println("Looking For Client_DataNode Registry");
			        			 Registry datanodeRegistry = LocateRegistry.getRegistry(datanodeIP);
			        			 String name1 = "Client_DataNode";
			        		     IDataNode idatanodeRMI = (IDataNode) datanodeRegistry.lookup(name1);
			        		     
			        		     System.out.println("Client_DataNode Lookup Successful");
			        		     
			        		     //read
			        		     System.out.println("Read Request at DataNode"+datanodeIP+" attempt "+readCount);
			        		     rbreqBuilder=HDFS.ReadBlockRequest.newBuilder();
			        		     rbreqBuilder.setBlockNumber(blockNumber);
			        		     rbresByte=idatanodeRMI.readBlock(rbreqBuilder.build().toByteArray());
			        		     rbres=HDFS.ReadBlockResponse.parseFrom(rbresByte);
			        		     
			        		     if(rbres.getStatus()==0)
			        		     {
			        		    	System.out.println("Block Read Request Successful "+blockNumber);
			        		    	System.out.print( new String(rbres.getData().toByteArray()));
			        		    	break;		        		    	
			        		     }
			        		     readCount--;
			        		    
			        		}while(readCount>0);
		        			 
		        			 if(readCount==0)
		        			 {
		        				 System.out.println("Block Read Error "+blockNumber);
		        				 break;
		        			 }
			        		  
		        		 }
		        		 break;
		        	 }
		        	 blCount--;
		         }
	        	 while(blCount>0);
	        	 if(blCount==0)
	        	 {
	        		 System.out.println("Block Location Error");
	        		 close(handle);
	        		 System.out.println("\nRead Failed\n");
	        	 }
	        	 else
	        	 {
		        	 //close
		        	 if(close(handle))
		        	 {
		        		 System.out.println("\nDone Reading Successfully\n");
		        	 }
		        	 else
		        	 {
		        		 System.out.println("\nDone Reading But Could Not Close File\n");
			         }
	        	 }
	         }
	         else
	         {
	        	 System.out.println("Open File Error");
	         }
		 }
		 catch (Exception e)
		 {
			 System.err.println("Exception In Reading");
	         e.printStackTrace();
		 }
		
	}
	
	private static HDFS.OpenFileResponse open(String fileName,boolean forRead) throws Exception
	{
		 //open
		int openCount=3;
		HDFS.OpenFileRequest.Builder ofreqBuilder= HDFS.OpenFileRequest.newBuilder();
		ofreqBuilder.setFileName(fileName);
	    ofreqBuilder.setForRead(forRead);
	    
		System.out.println("Open Request");
		HDFS.OpenFileResponse ofresp;
		do
		{
  			 System.out.println("Open Request Attempt:"+openCount);
  			 byte[] ofrespByte=inamenodeRMI.openFile(ofreqBuilder.build().toByteArray());
  			 ofresp=HDFS.OpenFileResponse.parseFrom(ofrespByte);
  			 if(ofresp.getStatus()==0)    
		    	return ofresp;
  			 openCount--;   
		}while(openCount!=0);
		
		return ofresp;
       
	}
	private static boolean close(int handle) throws Exception
	{
		 //close
	   	 System.out.println("Close Request");
		 HDFS.CloseFileRequest.Builder cfreqBuilder=HDFS.CloseFileRequest.newBuilder();
	   	 cfreqBuilder.setHandle(handle);
	   	 
	   	 int closeCount=3;
	   	
   		 do
	   	 {
   			 System.out.println("Close Request Attempt:"+closeCount);
	   		 byte[] cfrespByte=inamenodeRMI.closeFile(cfreqBuilder.build().toByteArray());
	   		 HDFS.CloseFileResponse cfresp=HDFS.CloseFileResponse.parseFrom(cfrespByte);
	       	 if(cfresp.getStatus()==0)
	       	 {
	       		 System.out.println("Close Request Successful");
	       		 return true;
	       	 }
	       	 closeCount--;
	   	 }while(closeCount>0);
	   	
   		 System.out.println("Close Request Error");
	   	 return false;
   	
	}
	
	private static void list()
	{
		try
		{
			System.out.println("\nIn List\n");
			//list
			System.out.println("List Request");
			HDFS.ListFilesRequest.Builder lfreqBuilder= HDFS.ListFilesRequest.newBuilder();
	     
	      
	       int listCount=3;
		   	
	   		 do
		   	 {
	   			 System.out.println("List Request Attempt:"+listCount);
	   			 byte[] lfrespByte=inamenodeRMI.list(lfreqBuilder.build().toByteArray());
	   			 HDFS.ListFilesResponse lfresp=HDFS.ListFilesResponse.parseFrom(lfrespByte);
	   			 if(lfresp.getStatus()==0)
		       	 {
	   				System.out.println("List Request Successful");
		        	System.out.println("Files Available At Server Are:");
		        	for(String file:lfresp.getFileNamesList())
		        	{
		        		System.out.println(file);
		        	}
		        	System.out.println("\nDone Listing Successfully\n");
		        	return;
		       	 }
	   			listCount--;
		   	 }while(listCount>0);
		   	
	   		 System.out.println("Listing Error");
		   	 
	   	
		}
		catch (Exception e) 
		{
			System.err.println("Exception In Listing");
			e.printStackTrace();
		}
	}
	
	private static String convertIntIp2String(int ip)
	{
		String ips="";
	
		
		ips=ip/16777216+".";
		ip=ip%16777216;
		
		ips+=ip/65536+".";
		ip=ip%65536;
		
		ips+=ip/256+".";
		ip=ip%256;
	
		ips+=ip;
		return ips;
	}
}
