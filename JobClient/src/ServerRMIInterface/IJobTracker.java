package ServerRMIInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IJobTracker extends Remote{
	
	/* JobSubmitResponse jobSubmit(JobSubmitRequest) */
	byte[]  jobSubmit(byte[] JobSubmitRequest)  throws RemoteException;;

	/* JobStatusResponse getJobStatus(JobStatusRequest) */
	byte[] getJobStatus(byte[] JobStatusRequest)  throws RemoteException;;
	
	/* HeartBeatResponse heartBeat(HeartBeatRequest) */
	byte[] heartBeat(byte[] HeartBeatRequest) throws RemoteException;;
}
